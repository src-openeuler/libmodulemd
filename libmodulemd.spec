# NOTICE: this spec file is retrieved from upper stream project(see libmodulemd.spec.in in Source0)
# and made some modifications.
Name:           libmodulemd
Version:        2.15.0
Release:        1
Summary:        C Library for manipulating module metadata files
License:        MIT
URL:            https://github.com/fedora-modularity/libmodulemd
Source0:        https://github.com/fedora-modularity/libmodulemd/releases/download/%{version}/modulemd-%{version}.tar.xz

BuildRequires:  meson pkgconfig gcc gcc-c++ gobject-introspection-devel libxslt
Buildrequires:  python3-devel python3-gobject-base glib2-doc rpm-devel file-devel help2man
%ifarch %{valgrind_arches}
BuildRequires: valgrind
%endif
BuildRequires:  pkgconfig(gobject-2.0) pkgconfig(gobject-introspection-1.0) pkgconfig(yaml-0.1) pkgconfig(gtk-doc)

%description
The libmodulemd API provides a number of convenience tools for 
interacting with repodata (that is, streams of YAML that contains 
information on multiple streams, default data and translations). 


%package devel
Summary:        Development files for libmodulemd
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development files for libmodulemd.

%package -n python3-%{name}
Summary:         python3 bindings for %{name}
Requires:        %{name} = %{version}-%{release} python3-gobject-base python3-six

%description -n python3-%{name} 
python3 bindings for %{name}

%prep
%autosetup -p1 -n modulemd-%{version}


%build
%meson -Dwith_py2=false
%meson_build


%check

export LC_CTYPE=C.utf8

%meson_test


%install
%meson_install


%ldconfig_scriptlets


%files
%license COPYING
%doc README.md
%{_bindir}/modulemd-validator
%{_libdir}/%{name}.so.2*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Modulemd*
%{_mandir}/man1/modulemd-validator*

%files devel
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/modulemd-2.0.pc
%{_includedir}/modulemd-2.0/
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Modulemd*
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/modulemd-2.0/

%files -n python3-%{name}
%{python3_sitearch}/gi/overrides/

%changelog
* Mon Jul 17 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 2.15.0-1
- Update to version 2.15.0

* Thu Nov 17 2022 fuanan <fuanan3@h-partners.com> - 2.14.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.14.0

* Thu Nov 17 2022 yixiangzhike <yixiangzhike007@163.com> - 2.13.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix source url

* Wed Jun 22 2022 fuanan <fuanan3@h-partners.com> - 2.13.0-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:fix build error:Unknow options:"developer_build"

* Tue Jul 27 2021 zoulin <zoulin13@huawei.com> - 2.13.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update libmodulemd version to 2.13.0

* Tue Apr 28 2020 zhouyihang <zhouyihang3@huawei.com> - 2.9.4-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update libmodulemd version to 2.9.4

* Fri Nov 22 2019 fangyufa<fangyufa1@huawei.com> - 1.6.4-4
- add buildrequires of libxslt(for x86_64) and disable valgrind test for build

* Mon Oct 28 2019 shenyangyang <shenyangyang4@huawei.com> - 1.6.4-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires of gobject-introspection-devel to solve build problem

* Thu Sep 05 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.4-2
- Package init
